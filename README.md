# Danesh Gostar
Danesh Gostar is a Persian web-based knowledge management system.  

# Features

### Search
Danesh Gostar provides special purpose search for every entity.

### Spam control
Danesh Gostar report system helps managers to fight with spams and harassment and find offenders. Danesh Gostar full logs prevent offenders from removing their traces.

### Customizable relationships
Danesh Gostar stores knowledge, sources, and the relationship between knowledge. Danesh Gostar allows companies to customize relationships in real time. The custom relationships lead to a complete overview of system knowledge and allow companies to extract knowledge flows from this overview. Besides relation based connection, Danesh Gostar provides tagging for easy access.

### Access Control
Danesh Gostar provides a hierarchy of users and allows different actions based on user access level, which facilitates knowledge organization.  
In Danesh Gostar, every object has its own access level. Danesh Gostar enforces this access on every view and create. This complete control over every user, knowledge, source, relation and tag provides full control over system's knowledge.  

### Request system
Danesh Gostar provides a request system which lets users request access to entities with a higher access level. 


### User activity reports
Danesh Gostar provides a report of user activities. Unlike many system Danesh Gostar tracks passive activities like viewing a page. Danesh Gostar reports provides insight on users needs with reporting their visited knowledge, which can be valuable data to improve users training and placement with learning algorithms.

# Support
Danesh Gostar is no longer supported.
